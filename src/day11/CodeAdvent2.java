package day11;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CodeAdvent2 {

    public static Long GALAXY_RATIO = 1000000L;

    public static void main(String[] args) {
        var input = Input.str;
        var separatedGalaxy = Arrays.stream(input.split("\n"))
                .map(r -> r.chars().mapToObj(c -> (char) c).collect(Collectors.toList()))
                .toList();

        List<Integer> noGalaxyCol = new ArrayList<>();
        List<Integer> noGalaxyRow = new ArrayList<>();

        for (var x = 0; x < separatedGalaxy.size(); x++) { //Row
            var containsCol = false;
            var containsRow = false;
            for (var y = 0; y < separatedGalaxy.get(x).size(); y++) { //Col
                if (separatedGalaxy.get(x).get(y) == '#') {
                    containsRow = true;
                }
                if (separatedGalaxy.get(y).get(x) == '#') {
                    containsCol = true;
                }
            }

            if (!containsRow) {
                noGalaxyRow.add(x);
            }

            if (!containsCol) {
                noGalaxyCol.add(x);
            }

        }

        AtomicInteger index = new AtomicInteger(0);
        var result = Arrays.stream(input.split("\n"))
                .map(l -> mapGalaxyPoint(l, index.getAndIncrement()))
                .flatMap(Collection::stream)
                .toList();

        var resultInt = IntStream.range(0, result.size())
                .mapToObj(x -> {
                    var galaxy1 = result.get(x);
                    return IntStream.range(x + 1, result.size())
                            .mapToLong(y -> {
                                var galaxy2 = result.get(y);
                                var distX = galaxy1.getKey() > galaxy2.getKey() ?
                                        calcDistance(galaxy1.getKey(), galaxy2.getKey(), noGalaxyCol)
                                        : calcDistance(galaxy2.getKey(), galaxy1.getKey(), noGalaxyCol);
                                var distY = calcDistance(galaxy2.getValue(), galaxy1.getValue(), noGalaxyRow);

                                return distX + distY;
                            })
                            .reduce(0L, Long::sum);

                })
                .reduce(0L, Long::sum);


        System.out.println(resultInt);
    }

    public static Long calcDistance(Integer high, Integer low, List<Integer> older) {
        var between = older.stream().filter(x -> x < high && x > low).count();
        return high - low - between + (between * GALAXY_RATIO);
    }

    public static List<Map.Entry<Integer, Integer>> mapGalaxyPoint(String line, int rowIdx) {
        List<Map.Entry<Integer, Integer>> galaxies = new ArrayList<>();
        var galaxyIndex = -1;
        do {
            galaxyIndex = line.indexOf("#", galaxyIndex + 1);
            if (galaxyIndex != -1) {
                galaxies.add(Map.entry(galaxyIndex, rowIdx));
            }

        } while (galaxyIndex != -1);


        return galaxies;
    }
}
