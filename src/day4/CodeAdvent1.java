package day4;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CodeAdvent1 {

    public static void main(String[] args) {
        var result = Arrays.stream(Input.str.split("\n"))
                .map(CodeAdvent1::doSomething)
                .reduce(0, Integer::sum);

        System.out.println(result);
    }

    public static Integer doSomething(String line) {
        var cards = line.split(":");
        var allNumbers = cards[1].split("\\|");

        var numbers = getNumbers(allNumbers[0]);
        var winningNumbers = new ArrayList<>(getNumbers(allNumbers[1]));

        return numbers.stream()
                .map(winningNumbers::remove)
                .filter(Boolean::booleanValue)
                .map(b -> 1)
                .reduce(0, (before, elem) -> before == 0 ? 1 : before * 2);
    }

    public static List<Integer> getNumbers(String line) {
        return Arrays.stream(line.split(" "))
                .filter(n -> n != "")
                .map(Integer::parseInt)
                .toList();
    }
}
