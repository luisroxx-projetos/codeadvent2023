package day6;


import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.stream.IntStream;

public class CodeAdvent1 {

    public static void main(String[] args) {
        var rows = Arrays.stream(Input.testStr.split("\n"))
                .toList();

        var pairs = IntStream.range(0, rows.size() - 1)
                .mapToObj(i -> {
                    var row = Arrays.stream(rows.get(i).split(" "))
                            .map(CodeAdvent1::tryParseInt)
                            .filter(Objects::nonNull)
                            .toList();
                    var row2 = Arrays.stream(rows.get(i + 1).split(" "))
                            .map(CodeAdvent1::tryParseInt)
                            .filter(Objects::nonNull)
                            .toList();

                    return IntStream.range(0, row.size())
                            .mapToObj(idx -> Map.entry(row.get(idx), row2.get(idx)))
                            .toList();
                })
                .flatMap(Collection::stream)
                .toList();

        var result = pairs
                .stream()
                .map(CodeAdvent1::findDifferences)
                .reduce(1, (s, e) -> s * e);

        System.out.println(result);
    }

    public static Integer tryParseInt(String s) {
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
            return null;
        }
    }

    public static Integer findDifferences(Map.Entry<Integer, Integer> timeDistance) {
        var time = timeDistance.getKey();
        var distance = timeDistance.getValue();

        var min = time / 2;
        var max = (time / 2) + 1;

        Integer distancePerMillis = 0;
        var i = min;
        do {
            distancePerMillis = (time - i) * i;
            if (distancePerMillis > distance) {
                min = i;
                i--;
            }

        } while (distancePerMillis > distance);

        i = max;
        do {
            distancePerMillis = (time - i) * i;
            if (distancePerMillis > distance) {
                max = i;
                i++;
            }

        } while (distancePerMillis > distance);


        return max - min + 1;
    }
}
