package tpl;


import java.util.Arrays;

public class CodeAdvent2 {

    public static void main(String[] args) {
        var result = Arrays.stream(Input.str.split("\n"))
                .map(CodeAdvent2::doSomething)
                .reduce(0, Integer::sum);

        System.out.println(result);
    }

    public static Integer doSomething(String line) {
        return 1;
    }
}
