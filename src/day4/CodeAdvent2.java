package day4;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class CodeAdvent2 {

    public static void main(String[] args) {
        var lines = Input.str.split("\n");

        AtomicInteger ai = new AtomicInteger(0);
        Map<Integer, Integer> extraProcess = new HashMap<>();
        IntStream.range(1, lines.length + 1).forEach(idx -> extraProcess.put(idx, 1));


        Arrays.stream(lines)
                .forEach(line -> {
                    var lineCard = ai.incrementAndGet();
                    var qt = extraProcess.get(lineCard);
                    var winningNumbers = CodeAdvent2.doSomething(line);

                    if (winningNumbers != 0) {
                        var initialRange = lineCard + 1;
                        IntStream
                                .range(initialRange, initialRange + winningNumbers)
                                .forEach(idx -> extraProcess.merge(idx, qt, Integer::sum));
                    }
                });

        var result = extraProcess
                .values()
                .stream()
                .reduce(0, Integer::sum);

        System.out.println(result);
    }

    public static Integer doSomething(String line) {
        var cards = line.split(":");
        var allNumbers = cards[1].split("\\|");

        var numbers = getNumbers(allNumbers[0]);
        var winningNumbers = new ArrayList<>(getNumbers(allNumbers[1]));

        return numbers.stream()
                .map(winningNumbers::remove)
                .filter(Boolean::booleanValue)
                .map(b -> 1)
                .reduce(0, (before, elem) -> before + 1);
    }

    public static List<Integer> getNumbers(String line) {
        return Arrays.stream(line.split(" "))
                .filter(n -> n != "")
                .map(Integer::parseInt)
                .toList();
    }
}
