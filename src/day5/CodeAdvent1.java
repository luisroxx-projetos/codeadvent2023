package day5;


import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class CodeAdvent1 {

    public static void main(String[] args) {
        var seeds = Arrays.stream(Input.seeds.split("\n"))
                .map(String::trim)
                .map(s -> Arrays.stream(s.split(" ")).toList())
                .flatMap(Collection::stream)
                .map(Long::parseLong)
                .toList();

        var soils = getData(Input.soil);

        var fertilizers = getData(Input.fertilizer);

        var water = getData(Input.water);

        var light = getData(Input.light);

        var temperature = getData(Input.temperature);

        var humidity = getData(Input.humidity);

        var location = getData(Input.location);

        var min = seeds.stream()
                .map(s -> findIn(s, soils))
                .map(s -> findIn(s, fertilizers))
                .map(s -> findIn(s, water))
                .map(s -> findIn(s, light))
                .map(s -> findIn(s, temperature))
                .map(s -> findIn(s, humidity))
                .map(s -> findIn(s, location))
                .min(Long::compare)
                .get();

        System.out.println(min);
    }

    public static List<Map.Entry<Long, Map.Entry<Long, Long>>> getData(String data) {
        return Arrays.stream(data.split("\n"))
                .map(CodeAdvent1::getMapNumbers)
                .toList();
    }


    public static Long findIn(Long seed, List<Map.Entry<Long, Map.Entry<Long, Long>>> lookup) {
        for (var entry : lookup) {
            var initialRange = entry.getKey();
            var destinationRange = entry.getValue().getKey();
            var rangeLength = entry.getValue().getValue();

            if (seed >= destinationRange && seed <= destinationRange + rangeLength - 1) {
                return (seed - destinationRange + initialRange);
            }
        }

        return seed;
    }

    public static Map.Entry<Long, Map.Entry<Long, Long>> getMapNumbers(String line) {
        var splitted = line.trim().split(" ");
        var initialRange = splitted[0];
        var destinationRange = splitted[1];
        var rangeLength = splitted[2];

        return Map.entry(Long.parseLong(initialRange), Map.entry(Long.parseLong(destinationRange), Long.parseLong(rangeLength)));

    }


}
