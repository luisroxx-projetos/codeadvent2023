package day9;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.IntStream;

public class CodeAdvent2 {

    public static void main(String[] args) {
        var result = Arrays.stream(Input.str.split("\n"))
                .map(CodeAdvent2::doSomething)
                .reduce(0L, Long::sum);

        System.out.println(result);
    }

    public static Long doSomething(String line) {
        List<Long> row = Arrays.stream(line.split(" ")).map(Long::parseLong).toList();
        var keepGoing = true;
        LinkedHashSet<Long> set = new LinkedHashSet<>();
        var sum = row.getFirst();
        var iter = 1;
        do {
            final var data = new ArrayList<>(row);
            row = IntStream
                    .range(0, data.size() - 1)
                    .mapToObj(idx -> mapInt(idx, data))
                    .toList();

            sum += iter % 2 == 0 ? row.getFirst() : -row.getFirst();

            set.clear();
            set.addAll(row);

            if (set.size() == 1 && set.getFirst() == 0L) {
                keepGoing = false;
            }
            iter++;
        } while (keepGoing);
        
        return sum;
    }

    public static Long mapInt(int idx, List<Long> numbers) {
        var one = numbers.get(idx);
        var two = numbers.get(idx + 1);

        return two - one;
    }
}
