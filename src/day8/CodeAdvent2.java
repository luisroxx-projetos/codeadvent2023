package day8;


import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CodeAdvent2 {

    public static void main(String[] args) {
        Map<String, Map.Entry<String, String>> map = new LinkedHashMap<>();

        Arrays.stream(Input.str.split("\n"))
                .forEach(line -> CodeAdvent2.fillMap(line, map));

        var str = Input.seq;
        var maxLength = str.length();
        var nodes = map.keySet().stream()
                .map(key -> key.endsWith("A") ? key : null)
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(Function.identity(), v -> v));

        var idx = 0;
        var loop = 0;
        Map<String, Integer> foundInMap = new HashMap<>();

        do {
            char direction = str.charAt(idx);
            idx++;
            if (idx == maxLength) {
                loop++;
                idx = 0;
            }
            for (var node : nodes.entrySet()) {
                var key = direction == 'L' ? map.get(node.getValue()).getKey() : map.get(node.getValue()).getValue();
                node.setValue(key);
                if (key.endsWith("Z")) {
                    foundInMap.putIfAbsent(node.getKey(), loop * maxLength + idx);
                }
            }

            if (foundInMap.size() == nodes.size()) {
                break;
            }
        } while (true);

        long lcm = CodeAdvent2.findLCM(foundInMap.values().stream().toList());

        System.out.println(lcm);
    }

    // Custom method to find the LCM of two numbers
    private static long findLCM(long a, long b) {
        return (a * b) / findGCD(a, b);
    }

    // Custom method to find the Greatest Common Divisor (GCD) of two numbers using Euclidean Algorithm
    private static long findGCD(long a, long b) {
        while (b > 0) {
            long temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }

    // Method to find the LCM of an array of numbers using Java streams
    private static long findLCM(List<Integer> numbers) {
        return numbers
                .stream()
                .mapToLong(Long::valueOf)
                .reduce(1, (a, b) -> (a * b) / findGCD(a, b));
    }

    public static void fillMap(String line, Map<String, Map.Entry<String, String>> map) {
        var spllitt1 = line.split(" = ");
        var key = spllitt1[0];

        var spllitt2 = spllitt1[1].split(", ");

        var left = spllitt2[0].replace("(", "").trim();
        var right = spllitt2[1].replace(")", "").trim();

        map.put(key, Map.entry(left, right));
    }
}
