package day7;


import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class CodeAdvent1 {

    public static void main(String[] args) {
        var atoi = new AtomicInteger(0);

        var result = Arrays.stream(Input.str.split("\n"))
                .map(CodeAdvent1::doSomething)
                .sorted((o1, o2) -> {
                    var elem1HandType = o1.getValue().getValue();
                    var elem2HandType = o2.getValue().getValue();

                    if (elem1HandType.equals(elem2HandType)) {
                        var elem1Hand = o1.getKey();
                        var elem2Hand = o2.getKey();

                        for (var i = 0; i < elem1Hand.length(); i++) {
                            var card1 = getCardValue(elem1Hand.charAt(i));
                            var card2 = getCardValue(elem2Hand.charAt(i));

                            if (!card1.equals(card2)) {
                                return card1.compareTo(card2);
                            }
                        }

                        //they are equals
                        return 0;
                    }

                    return elem1HandType.compareTo(elem2HandType);
                })
                .map(val -> val.getValue().getKey())
                .reduce(0, (sub, elem) -> sub + elem * atoi.incrementAndGet());

        System.out.println(result);
    }

    public static Map.Entry<String, Map.Entry<Integer, Integer>> doSomething(String line) {
        var split = line.split(" ");
        var hand = split[0];
        var bid = split[1];
        var handMap = normalizeHand(hand);
        var handType = calculateHandtype(handMap);

        return Map.entry(hand, Map.entry(Integer.parseInt(bid), handType));
    }

    public static Map<Character, Integer> normalizeHand(String hand) {
        var map = new HashMap<Character, Integer>();
        for (var c : hand.toCharArray()) {
            map.merge(c, 1, Integer::sum);
        }

        return map;
    }

    public static Integer calculateHandtype(Map<Character, Integer> hand) {
        var cards = hand.keySet();
        return switch (cards.size()) {
            case 1 -> 7;
            case 2 -> {
                //can be full house
                var firstValue = hand.values().stream().findFirst().orElseThrow();
                if (firstValue == 4 || firstValue == 1) {
                    yield 6;
                }


                yield 5;
            }
            case 3 -> {
                //can be two pair or three of kind\
                var hasThree = hand.values().stream().anyMatch(i -> i > 2);
                if (hasThree) {
                    yield 4;
                }

                yield 3;
            }
            case 4 -> 2;
            default -> 1;
        };

    }

    public static Integer getCardValue(Character card) {
        if (Character.isDigit(card)) {
            return Integer.parseInt("" + card);
        }

        return switch (card) {
            case 'T' -> 10;
            case 'Q' -> 12;
            case 'K' -> 13;
            default -> 14; //A
        };
    }
}
