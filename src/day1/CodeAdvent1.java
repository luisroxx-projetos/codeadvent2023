package day1;

import java.util.Arrays;
import java.util.Map;

public class CodeAdvent1 {
    static Map<String, Character> numbers = Map.ofEntries(
            Map.entry("one", '1'),
            Map.entry("two", '2'),
            Map.entry("three", '3'),
            Map.entry("four", '4'),
            Map.entry("five", '5'),
            Map.entry("six", '6'),
            Map.entry("seven", '7'),
            Map.entry("eight", '8'),
            Map.entry("nine", '9')
    );

    public static void main(String[] args) {
        var result = Arrays.stream(Input.str.split("\n"))
                .map(CodeAdvent1::lineToInt)
                .reduce(0, Integer::sum);

        System.out.println(result);
    }

    public static Integer lineToInt(String line) {
        char one = getChar(line, false);
        char two = getChar(line, true);
        return Integer.parseInt("" + one + two);
    }

    public static char getChar(String line, boolean inverse) {
        var lowest = numbers.entrySet()
                .stream()
                .map(entry ->
                        Map.entry(entry.getValue(),
                                inverse ? line.lastIndexOf(entry.getKey())
                                        : line.indexOf(entry.getKey()
                                )
                        )
                )
                .filter(n -> n.getValue() > -1)
                .reduce(Map.entry('?', inverse ? Integer.MIN_VALUE : Integer.MAX_VALUE), (subtotal, element) -> {
                    if (inverse) {
                        if (element.getValue() > subtotal.getValue()) {
                            return element;
                        }

                        return subtotal;
                    }

                    if (element.getValue() < subtotal.getValue()) {
                        return element;
                    }

                    return subtotal;
                });

        if (inverse) {
            for (int i = line.length() - 1; i >= 0; i--) {
                char c = line.charAt(i);
                if (Character.isDigit(c)) {
                    if (i > lowest.getValue()) {
                        return c;
                    }

                    return lowest.getKey();
                }
            }
        }

        for (int i = 0; i < line.length(); i++) {
            char c = line.charAt(i);
            if (Character.isDigit(c)) {
                if (i < lowest.getValue()) {
                    return c;
                }

                return lowest.getKey();
            }
        }

        throw new RuntimeException("eerror");
    }


}
