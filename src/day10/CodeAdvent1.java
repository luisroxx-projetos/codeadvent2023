package day10;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class CodeAdvent1 {

    public static final String YELLOW = "\033[0;33m";  // YELLOW
    public static final String YELLOW_BACKGROUND = "\033[43m"; // YELLOW
    public static final String RESET = "\033[0m";  // Text Reset

    public static void main(String[] args) {
        var input = Input.str;

        var idx = input.indexOf("S");
        var result = Arrays.stream(input.split("\n")).toList();
        var rowLength = result.get(0).length() + 1; //+1 for line break
        var row = idx / rowLength;
        var col = idx % rowLength;

        var nextWalk = Map.entry(0, -1);
        var steps = 0;
        var nextTile = 'S';
        List<Map.Entry<Integer, Integer>> walkList = new ArrayList<>();
        do {
            steps++;
            row += nextWalk.getValue();
            col += nextWalk.getKey();
            walkList.add(Map.entry(col, row));
            nextTile = result.get(row).charAt(col);

            nextWalk = next(nextTile, nextWalk);
        } while (nextTile != 'S');

        System.out.println(steps / 2);

        walkList.sort((a, bb) -> {
            return a.getKey().compareTo(bb.getKey());
        });
        walkList = walkList.reversed();

        var newLines = new ArrayList<>(result);

        walkList.forEach(kvp -> {
            var row1 = result.get(kvp.getValue());
            var val = row1.charAt(kvp.getKey());
            var edited = YELLOW_BACKGROUND + val + RESET;

            newLines.set(kvp.getValue(), row1.substring(0, kvp.getKey()) + edited + row1.substring(kvp.getKey() + 1));
        });

        newLines.forEach(l -> {
            System.out.print(l+"\n");
        });

//        System.out.println(newLines);

//        var abc = YELLOW_BACKGROUND + "mp" + RESET;
//        System.out.println(abc.length());
//        System.out.println(abc);


    }

//    public static String getAndReplace(List<String> lines, int row, int col) {
//        ConsoleColors.RED
//
//    }

    public static Map.Entry<Integer, Integer> next(char tile, Map.Entry<Integer, Integer> from) {
        if (tile == '|') {
            return from;
        }

        if (tile == '-') {
            return from;
        }

        if (tile == 'J') {
            if (from.getKey() == 1) {
                return Map.entry(0, -1);
            }

            return Map.entry(-1, 0);
        }

        if (tile == '7') {
            if (from.getKey() == 1) {
                return Map.entry(0, 1);
            }

            return Map.entry(-1, 0);
        }

        if (tile == 'F') {
            if (from.getKey() == -1) {
                return Map.entry(0, 1);
            }

            return Map.entry(1, 0);
        }

        if (tile == 'L') {
            if (from.getKey() == -1) {
                return Map.entry(0, -1);
            }

            return Map.entry(1, 0);
        }

        if (tile == 'S') {
            return Map.entry(0, 0);
        }

        throw new RuntimeException();
    }

}
