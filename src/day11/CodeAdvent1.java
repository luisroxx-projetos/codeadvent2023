package day11;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CodeAdvent1 {

    public static void main(String[] args) {
        var separatedGalaxy = Arrays.stream(Input.str.split("\n"))
                .map(r -> r.chars().mapToObj(c -> (char) c).collect(Collectors.toList()))
                .toList();

        var expandedGalaxy = separatedGalaxy.stream()
                .map(ArrayList::new)
                .collect(Collectors.toList());

        List<Character> cols = new ArrayList<>();
        List<Character> rows = new ArrayList<>();
        var addedRows = new AtomicInteger(0);
        var addedCols = new AtomicInteger(0);
        for (var x = 0; x < separatedGalaxy.size(); x++) { //Row
            rows.clear();
            cols.clear();
            for (var y = 0; y < separatedGalaxy.get(x).size(); y++) { //Col
                rows.add(separatedGalaxy.get(x).get(y));
                cols.add(separatedGalaxy.get(y).get(x));
            }

            if (!rows.contains('#')) {
                var value = addedRows.getAndIncrement();
                expandedGalaxy.add(x + value, new ArrayList<>(expandedGalaxy.get(x + value)));
            }

            if (!cols.contains('#')) {
                var value = addedCols.getAndIncrement();
                for (ArrayList<Character> expandedGalaxyRow : expandedGalaxy) {
                    var chararacter = expandedGalaxyRow.get(x + value);
                    expandedGalaxyRow.add(x + value, chararacter);
                }
            }
        }

        AtomicInteger index = new AtomicInteger(0);
        var result = expandedGalaxy.stream()
                .map(l -> l.stream().map(String::valueOf)
                        .reduce("", (acc, val) -> acc + val))
                .map(l -> doSomething(l, index.getAndIncrement()))
                .flatMap(Collection::stream)
                .toList();

        var resultInt = IntStream.range(0, result.size())
                .mapToObj(x -> {
                    var galaxy1 = result.get(x);
                    return IntStream.range(x + 1, result.size())
                            .mapToObj(y -> {
                                var galaxy2 = result.get(y);
                                var distX = galaxy1.getKey() > galaxy2.getKey() ? galaxy1.getKey() - galaxy2.getKey() : galaxy2.getKey() - galaxy1.getKey();
                                var distY = galaxy2.getValue() - galaxy1.getValue();

                                return distX + distY;
                            })
                            .reduce(0, Integer::sum);

                })
                .reduce(0, Integer::sum);


        System.out.println(resultInt);
    }

    public static List<Map.Entry<Integer, Integer>> doSomething(String line, int rowIdx) {
        List<Map.Entry<Integer, Integer>> galaxies = new ArrayList<>();
        var galaxyIndex = -1;
        do {
            galaxyIndex = line.indexOf("#", galaxyIndex + 1);
            if (galaxyIndex != -1) {
                galaxies.add(Map.entry(galaxyIndex, rowIdx));
            }

        } while (galaxyIndex != -1);


        return galaxies;
    }
}
