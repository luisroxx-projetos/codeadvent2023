package day8;


import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

public class CodeAdvent1 {

    public static void main(String[] args) {
        Map<String, Map.Entry<String, String>> map = new LinkedHashMap<>();

        Arrays.stream(Input.str.split("\n"))
                .forEach(line -> CodeAdvent1.fillMap(line, map));


        var str = Input.seq;
        var maxLength = str.length();
        String key = "AAA";

        var idx = 0;
        var loop = 0;
        do {
            char direction = str.charAt(idx);
            key = direction == 'L' ? map.get(key).getKey() : map.get(key).getValue();
            idx++;
            if (idx == maxLength) {
                loop++;
                idx = 0;
            }
        } while (!key.equals("ZZZ"));

        System.out.println(loop * maxLength + idx);
    }

    public static void fillMap(String line, Map<String, Map.Entry<String, String>> map) {
        var spllitt1 = line.split(" = ");
        var key = spllitt1[0];

        var spllitt2 = spllitt1[1].split(", ");

        var left = spllitt2[0].replace("(", "").trim();
        var right = spllitt2[1].replace(")", "").trim();

        map.put(key, Map.entry(left, right));
    }
}
