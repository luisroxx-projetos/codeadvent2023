package day3;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CodeAdvent2 {

    public static void main(String[] args) {
        var lines = Arrays.stream(Input.str.split("\n")).toList();
        var maxSize = lines.size();
        Integer sum = 0;
        for (int i = 0; i < lines.size(); i++) {
            String actualLine = getLineToProccess(i, lines, maxSize);
            String previousLine = getLineToProccess(i - 1, lines, maxSize);
            String nextLine = getLineToProccess(i + 1, lines, maxSize);

            sum += proccessLine(actualLine, previousLine, nextLine);
        }

        System.out.println(sum);
    }

    public static String getLineToProccess(int idx, List<String> lines, int maxSize) {
        if (idx < 0) {
            return null;
        }

        if (idx == maxSize) {
            return null;
        }

        return lines.get(idx);
    }

    public static Integer proccessLine(String line, String previousLine, String nextLine) {
        Integer sum = 0;

        for (var i = 0; i < line.length(); i++) {
            var character = line.charAt(i);
            if (character == '*') {
                var numbers = partNumbersAround(line, previousLine, nextLine, i);
                if (numbers.size() == 2) {
                    sum += numbers.stream().reduce(1, (subtotal, elem) -> subtotal * elem);
                }
            }

        }

        return sum;
    }

    public static List<Integer> partNumbersAround(String actualLine, String previousLine, String nextLine, Integer gearIdx) {
        var minPositionCheck = Math.max(gearIdx - 1, 0);
        var maxPositionCheck = Math.min((gearIdx + 1), actualLine.length() - 1);

        List<Integer> numbers = new ArrayList<>();
        lineLookup(previousLine, gearIdx, minPositionCheck, maxPositionCheck, numbers);
        lineLookup(nextLine, gearIdx, minPositionCheck, maxPositionCheck, numbers);
        lineLookup(actualLine, gearIdx, minPositionCheck, maxPositionCheck, numbers);
        return numbers;
    }

    private static void lineLookup(String line, Integer gearPosition, Integer initialPositionCheck, Integer maxPositionCheck, List<Integer> numbers) {
        if (line != null) {
            //LookBottom
            String centerNumber = digitsLoopkupForDirection(line, gearPosition, true, true);
            if (centerNumber != null) {
                numbers.add(Integer.parseInt(centerNumber));
            } else {
                //Look Diagonals
                String leftNumber = digitsLoopkupForDirection(line, initialPositionCheck, true, false);
                if (leftNumber != null) {
                    numbers.add(Integer.parseInt(leftNumber));
                }

                String rightNumber = digitsLoopkupForDirection(line, maxPositionCheck, false, true);
                if (rightNumber != null) {
                    numbers.add(Integer.parseInt(rightNumber));
                }
            }
        }
    }

    public static String digitsLoopkupForDirection(String line, int initialPositionCheck, boolean lookLeft, boolean lookRight) {
        var character = line.charAt(initialPositionCheck);
        if (Character.isDigit(character)) {
            var leftDigits = lookLeft ? lookDigitsLeft(line, initialPositionCheck) : "";
            var righDigits = lookRight ? lookDigitsRight(line, initialPositionCheck) : "";

            return leftDigits + character + righDigits;
        }

        return null;
    }

    public static String lookDigitsLeft(String line, Integer initialPositionCheck) {
        String leftDigits = "";
        var i = 1;
        char character = line.charAt(initialPositionCheck - i);
        while (Character.isDigit(character)) {
            leftDigits = character + leftDigits;
            i++;
            if (initialPositionCheck - i < 0) {
                break;
            }
            character = line.charAt(initialPositionCheck - i);
        }

        return leftDigits;
    }

    public static String lookDigitsRight(String line, Integer initialPositionCheck) {
        String rightDigits = "";
        var i = 1;
        char character = line.charAt(initialPositionCheck + i);
        while (Character.isDigit(character)) {
            rightDigits += character;
            i++;
            if (initialPositionCheck + i == line.length()) {
                break;
            }
            character = line.charAt(initialPositionCheck + i);
        }

        return rightDigits;
    }

}
