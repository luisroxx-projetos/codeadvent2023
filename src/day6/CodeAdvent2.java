package day6;


import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.stream.IntStream;

public class CodeAdvent2 {

    public static void main(String[] args) {
        var rows = Arrays.stream(Input.str.split("\n"))
                .toList();

        var pairs = IntStream.range(0, rows.size() - 1)
                .mapToObj(idx -> {
                    var row = Arrays.stream(rows.get(idx).split(" "))
                            .filter(i -> i != "" && !i.contains(":"))
                            .reduce("", (a, b) -> a + b);
                    var row2 = Arrays.stream(rows.get(idx + 1).split(" "))
                            .filter(i -> i != "" && !i.contains(":"))
                            .reduce("", (a, b) -> a + b);

                    return Map.entry(Long.parseLong(row), Long.parseLong((row2)));
                })
                .toList();

        var result = pairs
                .stream()
                .map(CodeAdvent2::findDifferences)
                .reduce(1L, (s, e) -> s * e);

        System.out.println(result);
    }

    public static Long findDifferences(Map.Entry<Long, Long> timeDistance) {
        var time = timeDistance.getKey();
        var distance = timeDistance.getValue();

        var min = time / 2;
        var max = (time / 2) + 1;

        Long distancePerMillis = 0L;
        var i = min;
        do {
            distancePerMillis = (time - i) * i;
            if (distancePerMillis > distance) {
                min = i;
                i--;
            }

        } while (distancePerMillis > distance);

        i = max;
        do {
            distancePerMillis = (time - i) * i;
            if (distancePerMillis > distance) {
                max = i;
                i++;
            }

        } while (distancePerMillis > distance);


        return max - min + 1;
    }
}
