package day5;


import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class CodeAdvent2 {


    public static void main(String[] args) {
        //This code took 12min, but it works I guess kkkkk
        var soils = getData(Input.soil);
        var fertilizers = getData(Input.fertilizer);
        var waters = getData(Input.water);
        var lights = getData(Input.light);
        var temperatures = getData(Input.temperature);
        var humiditys = getData(Input.humidity);
        var locations = getData(Input.location);

        var seeds = Arrays.stream(Input.seeds.split("\n"))
                .map(s -> s.split(" "))
                .toList();

        var min = Long.MAX_VALUE;

        for (var s : seeds) {
            var initial = Long.parseLong(s[0]);
            var range = Long.parseLong(s[1]);
            for (var i = initial; i < initial + range; i++) {
                var soil = findIn(i, soils);
                var fertilizer = findIn(soil, fertilizers);
                var water = findIn(fertilizer, waters);
                var light = findIn(water, lights);
                var temperature = findIn(light, temperatures);
                var humidity = findIn(temperature, humiditys);
                var location = findIn(humidity, locations);
                if (location < min) {
                    min = location;
                }
            }
        }

        System.out.println(min);
    }

    public static List<Map.Entry<Long, Map.Entry<Long, Long>>> getData(String data) {
        return Arrays.stream(data.split("\n"))
                .map(CodeAdvent1::getMapNumbers)
                .toList();
    }


    public static Long findIn(Long seed, List<Map.Entry<Long, Map.Entry<Long, Long>>> lookup) {
        for (var entry : lookup) {
            var initialRange = entry.getKey();
            var destinationRange = entry.getValue().getKey();
            var rangeLength = entry.getValue().getValue();

            if (seed >= destinationRange && seed <= destinationRange + rangeLength - 1) {
                return (seed - destinationRange + initialRange);
            }
        }

        return seed;
    }

    public static Map.Entry<Long, Map.Entry<Long, Long>> getMapNumbers(String line) {
        var splitted = line.trim().split(" ");
        var initialRange = splitted[0];
        var destinationRange = splitted[1];
        var rangeLength = splitted[2];

        return Map.entry(Long.parseLong(initialRange), Map.entry(Long.parseLong(destinationRange), Long.parseLong(rangeLength)));

    }
}
