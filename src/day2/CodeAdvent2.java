package day2;

import java.util.Arrays;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CodeAdvent2 {

    static Map<Pattern, Integer> patterns = Map.ofEntries(
            Map.entry(Pattern.compile("( \\b([1-9]|[12][0-9]|3[01])\\b red)"), 12),
            Map.entry(Pattern.compile("( \\b([1-9]|[12][0-9]|3[01])\\b green)"), 13),
            Map.entry(Pattern.compile("( \\b([1-9]|[12][0-9]|3[01])\\b blue)"), 14)
    );

    static Pattern gamePattern = Pattern.compile("(Game ([0-9]|[1-9][0-9]|[1-9][0-9][0-9]):)");

    public static void main(String[] args) {
        var result = Arrays.stream(Input.str.split("\n"))
                .map(CodeAdvent2::minSet)
                .reduce(0, Integer::sum);

        System.out.println(result);

    }

    public static Integer minSet(String line) {
        var min = 1;
        for (var entry : patterns.entrySet()) {
            min *= minForColor(entry.getKey(), line);
        }

        return min;
    }

    public static Integer minForColor(Pattern pattern, String line) {
        var min = Integer.MIN_VALUE;
        Matcher m = pattern.matcher(line);
        while (m.find()) {
            var splitted = m.group().trim().split(" ");
            var number = Integer.parseInt(splitted[0]);
            if (number > min) {
                min = number;
            }
        }

        return min;
    }
}
