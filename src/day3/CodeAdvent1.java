package day3;


import java.util.Arrays;
import java.util.List;

public class CodeAdvent1 {

    public static void main(String[] args) {
        var lines = Arrays.stream(Input.str.split("\n")).toList();
        var maxSize = lines.size();
        Integer sum = 0;
        for (int i = 0; i < lines.size(); i++) {
            String actualLine = getLineToProccess(i, lines, maxSize);
            String previousLine = getLineToProccess(i - 1, lines, maxSize);
            String nextLine = getLineToProccess(i + 1, lines, maxSize);
            if (nextLine == null) {
                System.out.println();
            }

            sum += proccessLine(actualLine, previousLine, nextLine);
        }

        System.out.println(sum);
    }

    public static String getLineToProccess(int idx, List<String> lines, int maxSize) {
        if (idx < 0) {
            return null;
        }

        if (idx == maxSize) {
            return null;
        }

        return lines.get(idx);
    }

    public static Integer proccessLine(String line, String previousLine, String nextLine) {
        Integer sum = 0;
        Integer numberInitialPos = null;
        boolean startCreatingNumber = false;
        String number = "";

        for (var i = 0; i < line.length(); i++) {
            var character = line.charAt(i);

            if (Character.isDigit(character)) {
                if (numberInitialPos == null) {
                    numberInitialPos = i;
                    startCreatingNumber = true;
                }

                number += character;
            }

            if (!Character.isDigit(character) || i == line.length() - 1) {
                if (startCreatingNumber) {
                    if (hasSimbol(line, previousLine, nextLine, number, numberInitialPos)) {
                        sum += Integer.parseInt(number);
                    }

                    //Reset everything
                    numberInitialPos = null;
                    startCreatingNumber = false;
                    number = "";
                }
            }

        }

        return sum;
    }

    public static boolean hasSimbol(String actualLine, String previousLine, String nextLine, String number, Integer numberInitialPos) {
        var initialPositionCheck = Math.max((numberInitialPos - 1), 0);
        var maxPositionCheck = Math.min((numberInitialPos + number.length()), actualLine.length() - 1);

        if (previousLine != null) {
            boolean hasSimbol = hasSimbolLine(previousLine, initialPositionCheck, maxPositionCheck);
            if (hasSimbol) {
                return true;
            }
        }

        if (nextLine != null) {
            boolean hasSimbol = hasSimbolLine(nextLine, initialPositionCheck, maxPositionCheck);
            if (hasSimbol) {
                return true;
            }
        }

        return hasSimbolLine(actualLine, initialPositionCheck, maxPositionCheck);
    }

    public static boolean hasSimbolLine(String line, int initialPositionCheck, int maxPositionCheck) {
        for (var i = initialPositionCheck; i <= maxPositionCheck; i++) {
            var character = line.charAt(i);
            if (!Character.isDigit(character) && character != '.') {
                return true;
            }
        }

        return false;
    }
}
