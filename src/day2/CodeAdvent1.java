package day2;

import java.util.Arrays;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CodeAdvent1 {

    static Map<Pattern, Integer> patterns = Map.ofEntries(
            Map.entry(Pattern.compile("( \\b([1-9]|[12][0-9]|3[01])\\b red)"), 12),
            Map.entry(Pattern.compile("( \\b([1-9]|[12][0-9]|3[01])\\b green)"), 13),
            Map.entry(Pattern.compile("( \\b([1-9]|[12][0-9]|3[01])\\b blue)"), 14)
    );

    static Pattern gamePattern = Pattern.compile("(Game ([0-9]|[1-9][0-9]|[1-9][0-9][0-9]):)");

    public static void main(String[] args) {
        var result = Arrays.stream(Input.str.split("\n"))
                .map(CodeAdvent1::isValidGame)
                .reduce(0, Integer::sum);

        System.out.println(result);
    }

    public static Integer isValidGame(String line) {
        for (var entry : patterns.entrySet()) {
            var isValid = isPatternValidForRow(entry.getKey(), line, entry.getValue());
            if (!isValid) {
                return 0;
            }
        }

        return retrieveGameId(line);
    }

    public static Integer retrieveGameId(String line) {
        Matcher m = gamePattern.matcher(line);
        while (m.find()) {
            var splitted = m.group().split(" ");
            return Integer.parseInt(splitted[1].replace(":", ""));
        }

        throw new RuntimeException();
    }

    public static boolean isPatternValidForRow(Pattern pattern, String line, int maxValid) {
        Matcher m = pattern.matcher(line);
        while (m.find()) {
            var splitted = m.group().trim().split(" ");
            if (Integer.parseInt(splitted[0]) > maxValid) {
                return false;
            }
        }

        return true;
    }
}
